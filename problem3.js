function filterAllUniqueLanguages(data) {
  if (Array.isArray(data)) {
    const uniqueLanguages = data.reduce((acc, currentItem) => {
      currentItem.languages.forEach((language) => {
        if (!acc.includes(language)) {
          acc.push(language);
        }
      });
      return acc;
    }, []);
    return uniqueLanguages;
  } else {
    return [];
  }
}
module.exports = filterAllUniqueLanguages;
