function userDetails(data) {
  if (Array.isArray(data)) {
    const details = data.map((user) => {
      let splittedMail = user.email.split("@");
      let splittedName = splittedMail[0].split(".");
      // console.log(splittedName);
      const firstName =
        splittedName[0].charAt(0).toUpperCase() +
        splittedName[0].slice(1).toLowerCase();
      const lastName =
        splittedName[1].charAt(0).toUpperCase() +
        splittedName[1].slice(1).toLowerCase();
      return {
        firstName: firstName,
        lastName: lastName,
        emailDomain: splittedMail[1],
      };
    });
    return details;
  } else {
    return [];
  }
}
module.exports = userDetails;
