function getAllProjectStatus(data) {
  if (Array.isArray(data)) {
    const projectStatus = data.reduce((acc, item) => {
      item.projects.forEach((project) => {
        if (!acc[project.status]) {
          acc[project.status] = [];
        }
        acc[project.status].push(project.name);
      });
      return acc;
    }, {});
    return projectStatus;
  } else {
    return [];
  }
}
module.exports = getAllProjectStatus;
